coala-bears-create
==================

    coala-bears-create is a simple CLI program which let's you scaffold
    a ``coala-bear`` in minutes. It asks you certain questions and based on your
    answers, a template for bears and tests is automatically
    generated for you. Now you just have to modify this template to add
    more specific details and you're good to go.

Note: Python 3.3+ only.

Installation
~~~~~~~~~~~~~
    For Windows:
      ``pip install coala-bears-create``

    For Linux:
      ``pip3 install coala-bears-create``

|asciicast|

License
~~~~~~~

    AGPL License

    `LICENSE included here <LICENSE>`__

.. |asciicast| image:: https://asciinema.org/a/49693.png
   :target: https://asciinema.org/a/49693
